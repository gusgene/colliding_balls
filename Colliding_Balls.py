import tkinter
import random

# constants
WIDTH = 640
HEIGHT = 480
BG_COLOR = 'white'
BAD_COLOR = 'red'
COLORS = ['aqua', 'fuchsia', BAD_COLOR, 'pink', 'yellow', BAD_COLOR, 'gold', 'chartreuse', BAD_COLOR]
ROOT_TITLE = 'Colliding Balls'
ZERO = 0
MAIN_BALL_COLOR = 'blue'
MAIN_BALL_RADIUS = 30
INT_DX = 1
INT_DY = 1
DELAY = 5



# Balls class
class Ball():
    def __init__(self, x, y, radius, color, dx = 0, dy = 0):
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color
        self.dx = dx
        self.dy = dy

    def draw(self):
        canvas.create_oval(self.x - self.radius, self.y - self.radius, self.x + self.radius, self.y + self.radius,
                           fill=self.color, outline=self.color)

    def hide(self):
        #canvas.delete(main_ball)
        canvas.create_oval(self.x - self.radius, self.y - self.radius, self.x + self.radius, self.y + self.radius,
                        fill=BG_COLOR, outline=BG_COLOR)

    def is_collision(self, ball):
        a = abs(self.x + self.dx - ball.x)
        b = abs(self.y + self.dy - ball.y)
        return (a * a + b * b) ** 0.5 <= self.radius + ball.radius

    def move(self):
        # colliding with vertical wall
        if (self.x + self.radius + self.dx >= WIDTH) or (self.x - self.radius + self.dx <= ZERO):
            self.dx = - self.dx
        if (self.y + self.radius + self.dy >= HEIGHT) or (self.y - self.radius + self.dy <= ZERO):
            self.dy = - self.dy
        # ther balls collision
        for ball in balls:
            if self.is_collision(ball):
                if ball.color != BAD_COLOR:  # not a bad ball
                    ball.hide()
                    balls.remove(ball)
                    self.dx = -self.dx
                    self.dy = -self.dy
                else:  # bad ball
                    self.dx = self.dy = 0
        self.hide()
        self.x += self.dx
        self.y += self.dy
        self.draw()



# mouse events
def mouse_click(event):
    global main_ball
    if event.num == 1:
        if 'main_ball' not in globals():
            main_ball = Ball(event.x, event.y, MAIN_BALL_RADIUS, MAIN_BALL_COLOR, INT_DX, INT_DY)
            main_ball.draw()
        else:
            if main_ball.dx * main_ball.dy > 0:
                main_ball.dy = -main_ball.dy
            else:
                main_ball.dx = -main_ball.dx
    elif event.num == 3:
        if main_ball.dx * main_ball.dy > 0:
            main_ball.dx = -main_ball.dx
        else:
            main_ball.dy = -main_ball.dy


def create_list_of_balls(number):
    lst = []
    while len(lst) < number:
        next_ball = Ball(random.choice(range(0, WIDTH)),
                         random.choice(range(0, HEIGHT)),
                         random.choice(range(15, 35)),
                         random.choice(COLORS))
        lst.append(next_ball)
        next_ball.draw()
    return lst


# main loop game
def main():
    if 'main_ball' in globals():
        main_ball.move()
    root.after(DELAY, main)


root = tkinter.Tk()
root.title(ROOT_TITLE)
canvas = tkinter.Canvas(root, width=WIDTH, height=HEIGHT, bg=BG_COLOR)
canvas.pack()
canvas.bind('<Button-1>', mouse_click) # left button of mouse
#canvas.bind('<Button-2>', mouse_click, '+') # center button of mouse
canvas.bind('<Button-3 >', mouse_click, '*') # right button of mouse
if 'main_ball' in globals():
    del main_ball
balls = create_list_of_balls(10)
main()
root.mainloop()
